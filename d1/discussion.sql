-- Add new records

-- Add artists
INSERT INTO artists (name) VALUES("Taylor Swift");
INSERT INTO artists (name) VALUES("Lady Gaga");
INSERT INTO artists (name) VALUES("Justin Bieber");
INSERT INTO artists (name) VALUES("Ariana Grande");
INSERT INTO artists (name) VALUES("Bruno Mars");

-- Add albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Fearless",
	"2008-1-1",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Red",
	"2012-1-1",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"A Star Is Born",
	"2018-1-1",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Born This Way",
	"2011-1-1",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Purpose",
	"2015-1-1",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Believe",
	"2012-1-1",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Dangerous Woman",
	"2016-1-1",
	6
);

-- Add songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Fearless",
	213,
	"Country pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Love Story",
	246,
	"Country pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"State of Grace",
	253,
	"Rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Red",
	204,
	"Country",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Born This Way",
	252,
	"Electropop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Sorry",
	152,
	"Dancehall",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Into You",
	242,
	"EDM",
	9
);

-- Advance Selects

-- Exclude records
SELECT * FROM songs where id !=1;

-- Greater than (or equal to)
SELECT * FROM songs WHERE id >= 5;

-- Less than (or equal to)
SELECT * FROM songs WHERE id <= 7;

-- Get specific songs via different search conditions (OR)
SELECT * FROM songs WHERE id = 1 OR song_name = "Red" OR length < 200;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 8, 9);
-- Note that IN will search for any value in a specified column. Unlike OR which can search multiple different columns.

-- Find partial matches
-- Start search from beginning of string
SELECT * FROM songs WHERE song_name LIKE "b%";
-- Start search from the end of a string
SELECT * FROM songs WHERE song_name LIKE "%s";
-- Search entire string
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- https://joins.spathon.com/




