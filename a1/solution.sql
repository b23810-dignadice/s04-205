-- Find all artists that has letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";
-- Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;
-- Join the "albums" and "songs" tables (only show the album name, song name and song length)
SELECT  albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;
-- Join the 'artists" and "albums" tables (fin all albums that has letter a in its name)
SELECT albums.album_title FROM artists
    JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";
-- Sort the albums in Z-A in order (show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
-- Join the albums and songs tables (sort albums from z to a)
SELECT * FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id
    LEFT JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;

